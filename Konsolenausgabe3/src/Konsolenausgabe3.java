
public class Konsolenausgabe3 {

	public static void main(String[] args) {
		
		System.out.printf("%s%2s %10s\n","Fahrenheit","|","Celsius");
		System.out.println("- - - - - - - - - - - - ");
		System.out.printf("%d%9s %10.2f\n",-20,"|",-28.8889);
		System.out.printf("%d%9s %10.2f\n",-10,"|",-23.3333);
		System.out.printf("%d%11s %10.2f\n",0,"|",-17.7778);
		System.out.printf("%d%10s %10.2f\n",+20,"|",-6.6667);
		System.out.printf("%d%10s %10.2f\n",+30,"|",-1.1111);

	}

}
