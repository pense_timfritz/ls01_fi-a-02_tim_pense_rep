
public class Konsolenausgabe2 {

	public static void main(String[] args) {
		
		System.out.printf("%d%s%5s %20s%5d\n", 0, "!", "=", "=", 0);
		System.out.printf("%d%s%5s %d%19s%5d\n", 1, "!", "=", 1,"=",1);
		System.out.printf("%d%s%5s %d %s %d%15s%5d\n", 2, "!", "=", 1,"*",2,"=",2);
		System.out.printf("%d%s%5s %d %s %d %s %d %10s%5d\n", 3, "!", "=", 1,"*",2,"*",3,"=",6);
		System.out.printf("%d%s%5s %d %s %d %s %d %s %d %6s%5d\n", 4, "!", "=", 1,"*",2,"*",3,"*",4,"=",24);
		System.out.printf("%d%s%5s %d %s %d %s %d %s %d %s %d %2s%5d\n", 5, "!", "=", 1,"*",2,"*",3,"*",4,"*",5,"=",120);

	}

}
