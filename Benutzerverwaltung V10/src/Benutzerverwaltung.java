import java.io.InputStream;
import java.util.Scanner;
import java.io.Console;

public class Benutzerverwaltung {

    public static void main(String[] args) {

        BenutzerverwaltungV10.start();

    }
}

class BenutzerverwaltungV10{
    public static void start(){
        BenutzerListe benutzerListe = new BenutzerListe();

        benutzerListe.insert(new Benutzer("Paula", "paula"));
        benutzerListe.insert(new Benutzer("Adam37", "adam37"));
        benutzerListe.insert(new Benutzer("Darko", "darko"));

        // Hier bitte das Menü mit der Auswahl
        //  - Anmelden
        //  - Registrieren
        // einfügen, sowie die entsprechenden Abläufe:
        // Beim Registrieren 2x das Passwort einlesen und vergleichen,
        // das neue Benutzerobjekt erzeugen und in die Liste einfügen.
        // Beim Anmelden (max. 3 Versuche) name und passwort einlesen,
        // in der Liste nach dem Namen suchen und das eingegebene Passwort
        // mit dem gespeicherten vergleichen.
        //System.out.println(benutzerListe.select());
        //System.out.println(benutzerListe.select("Peter"));
        //System.out.println(benutzerListe.select("Darko"));
        int i = 0;
        
        do
        {
        	System.out.println("Anmelden(a) oder Registrierung(r)?");
        	Scanner tastatur = new Scanner(System.in);
        	String auswahl = tastatur.next();
        	
        	if (auswahl.equals("a")) {Anmeldung(benutzerListe);i++;}
        	else if (auswahl.equals("r")) {Registrieren(benutzerListe);}
        	else {System.out.println("Ungültige Eingabe, du Schuft!");}
        	
        	//System.out.println(benutzerListe.select());
        }while (i==0);
        
        //System.out.println(benutzerListe.select());

        
    }
    
    public static void Anmeldung(BenutzerListe benutzerListe) {
    	
    	String[] q = new String[2];
    	String name = "";
    	String pw = "";
    	int i = 0;
    	
    	do {
    		System.out.print("Benutzername: ");
        	Scanner tastatur = new Scanner(System.in);
        	String n = tastatur.next();
        	
        	q = benutzerListe.select(n).split(" ");
        	if (q[0].equals(n)) { name = n; i++;}
        	else {System.out.println("Dieser Benutzer existiert nicht.");}

    	}while (i==0);
    	
    	boolean works = false;
    	for (int j= 0; j< 3; j++)
    	{
    		System.out.print("Passwort: ");
        	Scanner tastatur = new Scanner(System.in);
        	String n = tastatur.next();
        	
        	q = benutzerListe.select(name).split(" ");
        	if (q[1].equals(n)) { name = n; works = true; break;}
        	else {System.out.println("Falsches Passwort.");}
    	}
   
		for (int c = 0; c < 16; c++) {
			System.out.print("=");
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.print("\n\n");
    	
    	if (works) {System.out.println("Sie sind erfolgreich angemeldet.");}
    	else {System.out.println("Passwort zu oft falsch eingegeben");}
    }
    
    public static void Registrieren(BenutzerListe benutzerListe) {
    	
    	int i = 0;
    	String name = "";
    	String pw = "";
    	
    	do {
    	System.out.print("Benutzername: ");
    	Scanner tastatur = new Scanner(System.in);
    	String n = tastatur.next();
    	if (benutzerListe.select(n).equals(""))
    			{
    				name = n; i++;
    			}
    	else {System.out.println("Dieser Benutzername ist bereits vergeben, bitte versuche es erneut.");}
    	}while (i==0);
    	i=0;
    	do {
        	System.out.print("Passwort: ");
        	Scanner tastatur = new Scanner(System.in);
        	String p1 = tastatur.next();
        	System.out.print("Passwort wiederholen: ");
        	String p2 = tastatur.next();
        	if (p1.equals(p2))
        			{
        				pw = p1;
        				i++;
        			}
        	else {System.out.println("Missmatch!");}
        	}while (i==0);
    	
    		benutzerListe.insert(new Benutzer(name, pw));
    	
    		//return benutzerListe;
    }
}

class BenutzerListe{
    private Benutzer first;
    private Benutzer last;
    public BenutzerListe(){
        first = last = null;
    }
    public void insert(Benutzer b){
        // Sicherheitshalber setzen wir
        // den Nachfolger auf null:
        b.setNext(null);
        if(first == null){
            first = last = b;
        }
        else{
            last.setNext(b);
            last = b;
        }
    }
    public String select(){
        String s = "";
        Benutzer b = first;
        while(b != null){
            s += b.toString() + '\n';
            b = b.getNext();
        }
        return s;
    }
    public String select(String name){
        Benutzer b = first;
        while(b != null){
            if(b.hasName(name)){
                return b.toString();
            }
            b = b.getNext();
        }
        return "";
    }

    public boolean delete(String name){
        // ...
        return true;
    }
}

class Benutzer{
    private String name;
    private String passwort;

    private Benutzer next;

    public Benutzer(String name, String pw){
        this.name = name;
        this.passwort = pw;

        this.next = null;
    }

    public boolean hasName(String name){
        return name.equals(this.name);
    }

    public String toString(){
        String s = "";
        s += name + " ";
        s += passwort;
        return s;
    }

    public Benutzer getNext(){
        return next;
    }

    public void setNext(Benutzer b){
        next = b;
    }
}



