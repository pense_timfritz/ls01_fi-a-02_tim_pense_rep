import java.util.Scanner;

public class Temparaturumrechnung {

	public static void main(String[] args) {

		tempRechner();
		
	}
	
	public static void tempRechner() {
		
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Bitte den Startwert eingeben: ");
		double startwert = tastatur.nextDouble();
		System.out.println("");
		System.out.print("Bitte den Endwert eingeben: ");
		double endwert = tastatur.nextDouble();
		System.out.println("");
		System.out.print("Bitte die Schrittweise eingeben: ");
		double schritt = tastatur.nextDouble();
		System.out.println("");
		
		System.out.println("T in C           T in F");
		System.out.println("________________________");
		if (startwert < endwert) {
		for (double i = startwert; i<=endwert; i += schritt) {
			
			double fahrenheit = (i * 9/5)+32;
			
			System.out.printf("%6.2f %15.2f\n", i, fahrenheit);
			
		}
		}
		
	}

}
