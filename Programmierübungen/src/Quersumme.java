import java.util.Scanner;

public class Quersumme {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Bis wo soll gezählt werden?");
		int zahl = tastatur.nextInt();
		System.out.println("Quersumme von "+zahl+" ist "+summeBilden(zahl));

	}
	
	public static int summeBilden(int j) {
		
		if (j <= 9) return j;

		return j%10 + summeBilden(j/10);
		
		
	}

}
