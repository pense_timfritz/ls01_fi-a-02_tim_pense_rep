
public class Modulo {

	public static void main(String[] args) {
		durchSieben();
		durchFuenfNichtVier();

	}
	
	public static void durchSieben() {
		
		System.out.println("Durch 7 teilbar sind:");
		for (int i = 1; i<=200; i++) {
			
			if (i % 7 == 0) {
				System.out.print(i+",");
			}
			
		}
		System.out.println("");
		
	}
	
	public static void durchFuenfNichtVier() {
		
		System.out.println("Durch 5 aber nicht durch 4 teilbar sind:");
		
		for (int i = 0; i <= 200; i++) {
			
			if (i % 5 == 0 && i % 4 != 0) {
				System.out.print(i+",");
			}
			
		}
		
	}

}
