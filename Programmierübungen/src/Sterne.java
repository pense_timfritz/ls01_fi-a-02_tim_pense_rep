import java.util.Scanner;

public class Sterne {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Wie viele Stufen?");
		sternenPyramide(tastatur.nextInt());
	}
	
	public static void sternenPyramide(int j) {
		
		
		for (int i = 1; i <= j; i++) {
			
			for (int b = i; b > 0; b--) {
				System.out.print("*");
				
			}
			System.out.println("");
		}
		
	}

}
