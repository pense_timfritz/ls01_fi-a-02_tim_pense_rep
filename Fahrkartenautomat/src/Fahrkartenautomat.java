﻿import java.security.DrbgParameters.NextBytes;
import java.util.Scanner;

public class Fahrkartenautomat {

	public static void main(String[] args) {

	boolean abfrage= true;
	
	while (abfrage = true){
		double zuZahlen = fahrkartenbestellungErfassen();
		double rückgabebetrag = fahrkartenBezahlen(zuZahlen);

		fahrkartenAusgeben();
		rueckgeldAusgeben(rückgabebetrag);
		
		abschied();
		System.out.println("nächster Kunde?   (j/n)");
		Scanner abfragen = new Scanner(System.in);
		char antwortString = abfragen.next().charAt(0);
		if (antwortString == 'n') {
	
		
		break;
		}
	}
	
	}
	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Fahrkartenauswahl (bitte Zahl auswählen) :");
		System.out.println("  Einzelfahrschein (1)");
		System.out.println("  Tageskarte (2)");
		System.out.println("  Kleingruppen-Tageskarte (3)");
		System.out.println("");
		System.out.println("");
		double zuZahlenderBetrag = 0;
		System.out.print("Ihre Wahl :");
		int wahl = tastatur.nextInt();
		
		
		if (wahl < 1 || wahl > 3) {System.out.println("Wahl ungültig bitte nochmal versuchen."); wahl = wahlWH();}
		if (wahl == 1) {zuZahlenderBetrag = 2.90;}
		if (wahl == 2) {zuZahlenderBetrag = 8.60;}
		if (wahl == 3) {zuZahlenderBetrag = 23.50;}
		
		
		
		System.out.print("Anzahl der Tickets?: ");
		int ticketanzahl = tastatur.nextInt();
		double zuZahlen = zuZahlenderBetrag;
		
		
		if (ticketanzahl > 0 && ticketanzahl <= 10) {
			zuZahlen = zuZahlenderBetrag * ticketanzahl;
		}
		
		else {
			System.out.println("Die Eingabe war ungültig. Bitte erneut versuchen.");
				ticketanzahl = anzahlDerTicketsWH();
				zuZahlen = zuZahlenderBetrag * ticketanzahl;
		}
		
		System.out.println ("Möchten sie noch weitere Tickets?j/n");
		char antwort = tastatur.next().charAt(0);
		
		if (antwort == 'j') {double zusätzlicherPreis = andereTickets(); zuZahlen += zusätzlicherPreis;}
		
		
		
		return zuZahlen;
	}
	
	public static double andereTickets() {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Fahrkartenauswahl (bitte Zahl auswählen) :");
		System.out.println("  Einzelfahrschein (1)");
		System.out.println("  Tageskarte (2)");
		System.out.println("  Kleingruppen-Tageskarte (3)");
		System.out.println("");
		System.out.println("");
		double zuZahlenderBetrag = 0;
		System.out.print("Ihre Wahl :");
		int wahl = tastatur.nextInt();
		
		
		if (wahl < 1 || wahl > 3) {System.out.println("Wahl ungültig bitte nochmal versuchen."); wahl = wahlWH();}
		if (wahl == 1) {zuZahlenderBetrag = 2.90;}
		if (wahl == 2) {zuZahlenderBetrag = 8.60;}
		if (wahl == 3) {zuZahlenderBetrag = 23.50;}
		
		
		
		System.out.print("Anzahl der Tickets?: ");
		int ticketanzahl = tastatur.nextInt();
		double zuZahlen = zuZahlenderBetrag;
		
		
		if (ticketanzahl > 0 && ticketanzahl <= 10) {
			zuZahlen = zuZahlenderBetrag * ticketanzahl;
		}
		
		else {
			System.out.println("Die Eingabe war ungültig. Bitte erneut versuchen.");
				ticketanzahl = anzahlDerTicketsWH();
				zuZahlen = zuZahlenderBetrag * ticketanzahl;
		}
		
		System.out.println ("Möchten sie noch weitere Tickets?j/n");
		char antwort = tastatur.next().charAt(0);
		
		if (antwort == 'j') {double zusätzlicherPreis = andereTickets(); zuZahlen += zusätzlicherPreis;}
		
		
		return zuZahlen;
	}
	
	public static int anzahlDerTicketsWH() {
		
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Anzahl der Tickets?: ");
		int ticketanzahl = tastatur.nextInt();
		if (ticketanzahl > 0 && ticketanzahl <= 10) {
			return ticketanzahl;
		}
		else {
			System.out.println("Die Eingabe war ungültig. Bitte erneut versuchen.");
			anzahlDerTicketsWH();
		}
		return ticketanzahl;
		
	}
	
	public static int wahlWH() {
		
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Fahrkartenauswahl (bitte Zahl auswählen) :");
		System.out.println("  Einzelfahrschein (1)");
		System.out.println("  Tageskarte (2)");
		System.out.println("  Kleingruppen-Tageskarte (3)");
		System.out.println("");
		System.out.println("");
		System.out.print("Ihre Wahl :");
		int wahl = tastatur.nextInt();
		
		if (wahl == 1) {return wahl;}
		else if (wahl == 2) {return wahl;}
		else if (wahl == 3) {return wahl; }
		else {System.out.println("Wahl ungültig bitte nochmal versuchen."); wahl = wahlWH();}
		
		
		
		return wahl;
	}

	public static double fahrkartenBezahlen(double zuZahlen) {
		Scanner tastatur = new Scanner(System.in);
		double eingeworfeneMünze;
		double eingezahlterGesamtbetrag = 0.00D;
		double rückgabebetrag;

		while (eingezahlterGesamtbetrag < zuZahlen) {
			System.out.printf("Noch zu zahlen: %.2f%s\n ", (zuZahlen - eingezahlterGesamtbetrag), " EURO");

			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;

		}
		rückgabebetrag = eingezahlterGesamtbetrag - zuZahlen;
		return rückgabebetrag;
	}

	public static void fahrkartenAusgeben() {

		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double rückgabebetrag) {

		//System.out.println(rückgabebetrag);	Rundungstest

		rückgabebetrag = Math.round(100.0 * rückgabebetrag);
		rückgabebetrag = rückgabebetrag / 100;

		if (rückgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f%s ", rückgabebetrag, " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
				rückgabebetrag = Math.round(100.0 * rückgabebetrag);
				rückgabebetrag = rückgabebetrag / 100;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
				rückgabebetrag = Math.round(100.0 * rückgabebetrag);
				rückgabebetrag = rückgabebetrag / 100;

			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
				rückgabebetrag = Math.round(100.0 * rückgabebetrag);
				rückgabebetrag = rückgabebetrag / 100;

			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
				rückgabebetrag = Math.round(100.0 * rückgabebetrag);
				rückgabebetrag = rückgabebetrag / 100;

			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
				rückgabebetrag = Math.round(100.0 * rückgabebetrag);
				rückgabebetrag = rückgabebetrag / 100;

			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
				rückgabebetrag = Math.round(100.0 * rückgabebetrag);
				rückgabebetrag = rückgabebetrag / 100;

			}
		}

	}

	public static void abschied() {
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"
							+ "vor Fahrtantritt entwerten zu lassen!\n"
							+ "Wir wünschen Ihnen eine gute Fahrt.");
	}
}